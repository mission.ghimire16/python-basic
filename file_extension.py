import os

parent_path="/media/mission/1545e26a-cee6-4082-a827-8a491d3c1c0e/python/python and flask/python/project/parent"
count_jpg=0
count_jpeg=0
count_txt=0
count_other=0

for child_dir in os.listdir(parent_path):
    
    child_path=os.path.join(parent_path,child_dir)
    count_jpg=0
    count_txt=0
    count_other=0
    # print(child_path)
    for child_item in os.listdir(child_path):
        child_item_path=os.path.join(child_path,child_item)
        file_path=child_item_path.split('.')

        if file_path[-1]=="txt":
            count_txt+=1
        elif file_path[-1]=="jpeg":
            count_jpeg+=1
        elif file_path[-1]=="jpg":
            count_jpg+=1

        else:
            count_other+=1
            
    print(f'{child_dir} txt_file: {count_txt}, jpeg : {count_jpeg}, jpg: {count_jpg} ,other:{count_other}')
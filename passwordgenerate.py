import string
import random

# alphabets=list(string.ascii_letters)
# digits=list(string.digits)
# special_char=list("!@#$%^&*()_-+=][.")
# characters=list()
characters = list(string.ascii_letters + string.digits + "!@#$%^&*()'=+-_.;:")


def generate_random():
    length = int(input("enter a number"))
    random.shuffle(characters)
    password = []
    for i in range(length):
        password.append(random.choice(characters))
    random.shuffle(password)
    print("".join(password))


generate_random()

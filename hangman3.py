import random

word_list = ["aardvark", "baboon", "camel"]
choose = random.choice(word_list)
print(f'choosen word {choose}')

display = []
word_length = len(choose)

for _ in range(word_length):
    display += '_'
end_of_game = False
while not end_of_game:
    guess_word = input("guess a letter:-").lower()

    for position in range(word_length):
        letter = choose[position]
        if letter == guess_word:
            display[position] = letter
    print(display)
    if "_" not in display:
        end_of_game = True
        print("You win.")

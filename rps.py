import random

user_count = 0
comp_count = 0
while True:
    user = input("Enter choice  rock,  paper, and  scissor \n")
    choose = ["rock", "paper", "scissors"]
    comp_random = random.choice(choose)
    cw = "computer won"
    uw = "user win"

    print(f"user choice= {user} and computer Choice={comp_random}")

    if user=="rock" and comp_random=="scissors" or\
            user=="scissors"   and comp_random=="paper" or\
            user=="paper" and comp_random=="rock":
        print(uw)
        user_count += 1

    elif comp_random=="rock" and user=="scissors" or\
        comp_random=="scissors"   and user=="paper" or\
        comp_random=="paper" and user=="rock":
        print(cw)
        comp_count += 1

    else:
        if user == comp_random:
            print("its tie")
            user_count += 1
            comp_count += 1

    print(f'comuter score:{comp_count}-your score:{user_count}')

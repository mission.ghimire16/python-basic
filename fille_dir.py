import os

count_folder = 0
count_image = 0
count_childfolder = 0

parent_dir = "/media/mission/1545e26a-cee6-4082-a827-8a491d3c1c0e/python/python and flask/python/project/dir"

for child_dirs in os.listdir(parent_dir):
    child_path = os.path.join(parent_dir, child_dirs)
    # print(child_dirs)
    count_folder = 0
    count_image = 0
    for child_items in os.listdir(child_path):
        childs_img = os.path.join(child_path, child_items)

        if os.path.isdir(childs_img):
            count_folder += 1

        elif os.path.isfile(childs_img):
            count_image += 1

    print(f"{ child_dirs } folder :- {count_folder} image:-{count_image}")
